package com.example.demo.service;

import com.example.demo.constant.KafkaConstant;
import com.example.demo.dto.TransferRequestDto;
import com.example.demo.util.ObjectMapperService;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class KafkaService {
    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    @Autowired
    private ObjectMapperService objectMapperService;

    @KafkaListener(topics = KafkaConstant.TOPIC_TRANSFER, groupId = KafkaConstant.GROUP_ID)
    public void consumeTransfer(String jsonData) throws JsonProcessingException {
        var a = objectMapperService.jsonToObject(jsonData, TransferRequestDto.class);
        // make transfer
        log.info(String.format("#### -> Consumed message-> %s", jsonData));
    }

    public void sendMessageTransfer(String smsJsonData) {
        log.info(String.format("#### -> Producing message -> %s", smsJsonData));
        this.kafkaTemplate.send(KafkaConstant.TOPIC_TRANSFER, smsJsonData);
    }
}
