package com.example.demo.dto;

import com.example.demo.enums.AccountType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@Getter
@Setter
public class TransferRequestDto {
    @NotBlank
    @Size(max = 50)
    private String request_id;

    @NotBlank
    @Size(max = 20)
    private String partner_id;

    @NotBlank
    @Size(max = 20)
    private String bank_no;

    @NotBlank
    @Size(max = 22)
    private String account_no;

    @NotNull
    private AccountType account_type;

    @NotBlank
    @Size(max = 164)
    private String account_name;

    @NotNull
    @Positive
    private Integer amount;

    @NotBlank
    @Size(max = 100)
    private String content;

    @NotBlank
    @Size(max = 200)
    private String signature;
}
