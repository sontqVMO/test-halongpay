package com.example.demo.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class ObjectMapperService {
    private static final ObjectMapper objectMapper = new ObjectMapper();

    public <T> T jsonToObject(String json, Class<T> aClass) throws JsonProcessingException {
        return objectMapper.readValue(json, aClass);
    }

    public String objectToJson(Object o) throws JsonProcessingException {
        return objectMapper.writeValueAsString(o);
    }
}
