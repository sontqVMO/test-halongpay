package com.example.demo.controller;

import com.example.demo.constant.KafkaConstant;
import com.example.demo.dto.TransferRequestDto;
import com.example.demo.service.KafkaService;
import com.example.demo.util.ObjectMapperService;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Properties;

@RestController
public class TestController {
    @Autowired
    private KafkaService kafkaService;
    @Autowired
    private ObjectMapperService objectMapperService;

    @PostMapping("/transfer")
    public ResponseEntity<?> sendTransferCommand(@Valid @RequestBody TransferRequestDto request) throws JsonProcessingException {
        kafkaService.sendMessageTransfer(objectMapperService.objectToJson(request));

        return ResponseEntity.ok().body("");
    }
}
