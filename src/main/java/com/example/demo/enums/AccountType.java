package com.example.demo.enums;

import lombok.Getter;

public enum AccountType {
    _0(0, "Bank account number"),
    _1(1, "Bank card number");

    @Getter
    private Integer id;
    @Getter
    private String name;

    AccountType(Integer id, String name) {
        this.id = id;
        this.name = name;
    }
}
